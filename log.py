import json
import sqlite3

# define paths
db_path = '/home/dan/dump1090-log/dump1090.db'
dump1090_path = '/run/dump1090-fa/aircraft.json'

def insert_log(data_tuple):
    with sqlite3.connect(db_path) as db:
        cursor = db.cursor()

        # create database tables 
        cursor.execute("""CREATE TABLE IF NOT EXISTS staging (log_date TEXT, hex_code TEXT, flight_number TEXT, 
        barometric_altitude REAL, geometric_altitude REAL, ground_speed REAL, indicated_air_speed REAL, true_air_speed REAL, magnetic_heading REAL, 
        true_heading REAL, latitude REAL, longitude REAL, messages INTEGER, last_seen INTEGER, rssi REAL);""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS message_log (log_date TEXT, hex_code TEXT, flight_number TEXT, 
        barometric_altitude REAL, geometric_altitude REAL, ground_speed REAL, indicated_air_speed REAL, true_air_speed REAL, magnetic_heading REAL, 
        true_heading REAL, latitude REAL, longitude REAL, messages INTEGER, last_seen INTEGER, rssi REAL);""")

        # insert new messages into staging table
        sql_insert_with_param = """INSERT INTO staging (log_date,hex_code,flight_number,barometric_altitude,
        geometric_altitude,ground_speed,indicated_air_speed,true_air_speed,magnetic_heading,true_heading,latitude,longitude,messages,last_seen,rssi) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
        
        cursor.execute(sql_insert_with_param, data_tuple)

        # insert messages from staging into message_log 
        cursor.execute("""INSERT INTO message_log SELECT datetime(log_date,'unixepoch', 'localtime') as log_date,hex_code,flight_number,barometric_altitude,
        geometric_altitude,ground_speed,indicated_air_speed,true_air_speed,magnetic_heading,true_heading,latitude,longitude,messages,last_seen,rssi from staging;""")

        # drop staging table
        cursor.execute("""DROP TABLE IF EXISTS staging;""")

        # close database connection
        cursor.close()

def main():
    # open aircraft.json file and parse json message data. See https://github.com/SDRplay/dump1090/blob/master/README-json.md
    with open(dump1090_path) as f:
        parsed_data = json.load(f)
        timestamp = parsed_data['now']

        # keys are omitted if data is not available. Replace omitted keys with blank variables.
        for each in parsed_data['aircraft']:
            if 'hex' in each:
                hex_code = each['hex']
            else:
                hex_code = ''

            if 'flight' in each:
                flight_number = each['flight']
            else: 
                flight_number = ''

            if 'alt_baro' in each:
                barometric_altitude = each['alt_baro']   
            else:
                barometric_altitude = ''

            if 'alt_geom' in each:
                geometric_altitude = each['alt_geom']
            else:
                geometric_altitude = ''

            if 'gs' in each:
                ground_speed = each['gs']
            else:
                ground_speed = ''
                
            if 'ias' in each:
                indicated_air_speed = each['ias'] 
            else:
                indicated_air_speed = ''

            if 'tas' in each:
                true_air_speed = each['tas'] 
            else:
                true_air_speed = ''

            if 'mag_heading' in each:
                magnetic_heading = each['mag_heading'] 
            else:
                magnetic_heading = ''

            if 'true_heading' in each:
                true_heading = each['true_heading'] 
            else:
                true_heading = ''

            if 'lat' in each:
                latitude = each['lat'] 
            else:
                latitude = ''

            if 'lon' in each:
                longitude = each['lon'] 
            else:
                longitude = ''

            if 'messages' in each:
                messages = each['messages'] 
            else:
                messages = ''

            if 'seen' in each:
                last_seen = each['seen']      
            else:
                last_seen = ''

            if 'rssi' in each:
                rssi = each['rssi'] 
            else:
                rssi = ''

            data_tuple = (timestamp, hex_code, flight_number, barometric_altitude, geometric_altitude, ground_speed, indicated_air_speed, true_air_speed, magnetic_heading, true_heading, latitude, longitude, messages, last_seen, rssi)
            insert_log(data_tuple)

if __name__ == '__main__':
    main()
