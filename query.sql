select strftime('%d-%m-%Y', log_date) as log_date, hex_code, count(*) as no_of_log_messages
from message_log
group by 1,2
having count(*) > 0 
order by log_date, count(*) desc;